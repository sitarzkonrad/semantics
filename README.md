### Prerequisites

- docker
- docker-compose

### Start

`docker-compose up`

App will start at http://localhost:5002/ \\
Flower is available on http://localhost:5555/ \\
Mongoengine is on http://localhost:8765/ \\

### Tests

1. `docker-compose up -d`
2. `docker-compose exec app ash`
3. `python -m pytest`
