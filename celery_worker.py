#!/usr/bin/env python
import os
from app import create_app
from crawler.tasks import celery


if __name__ == "__main__":
    app = create_app()
    app.app_context().push()