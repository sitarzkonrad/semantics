from flask import Flask
from flask_restplus import Api
import urllib.request
from urllib.request import URLopener
from crawler.routers import image, text, status
from crawler.database import create_connection
from crawler.tasks import celery


def create_app():
    app = create_server()
    create_api(app)
    create_connection()
    init_opener()
    celery.conf.update(app.config)
    return app


def create_server():
    app = Flask(__name__, static_url_path='')
    app.url_map.strict_slashes = False
    return app


def create_api(app):
    api = Api(
        title='Scraper',
        version='1.0',
        description='A description',
    )
    api.add_namespace(image.api)
    api.add_namespace(text.api)
    api.add_namespace(status.api)
    api.init_app(app)


def init_opener():
    opener = urllib.request.build_opener()
    opener.addheaders = [('User-agent', 'Mozilla/5.0')]
    urllib.request.install_opener(opener)

if __name__ == '__main__':
    app = create_app()
    app.run(host='0.0.0.0', port=5000, debug=True)