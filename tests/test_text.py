import pytest
import json
import responses
from tests.helpers import page_mock
from crawler.controllers.text import post_text, get_text
from crawler.services.text import download_text
from urllib.parse import quote


@pytest.mark.parametrize('url', [
    'https://en.wikipedia.org/wiki/Gecko',
])
def test_crawl_text(celery_app, client, url):
    responses.add(responses.GET, url,
                  body=b'', status=200)
    res = client.post('/text', data=json.dumps(dict(url=url)),
                      content_type='application/json')
    assert res.status_code == 200


@pytest.mark.parametrize('url, html, text', [
    ('https://example.com/', page_mock.html, page_mock.text)
])
def test_response_crawl_text(url, html, text):
    crawled_text = download_text(url)
    assert crawled_text['text'] == text


@pytest.mark.parametrize('url', [
    'blahwbla12312dp'
])
def test_crawl_text_with_invalid_url(client, url):
    res = client.post('/text', data=json.dumps(dict(url=url)),
                      content_type='application/json')
    assert res.status_code == 400
    assert res.json == {'errors': {'url': 'blahwbla12312dp is not a valid URL'},
                        'message': 'Input payload validation failed'}


@pytest.mark.parametrize('url, html, expected_text', [
    ('https://example.com/', page_mock.html, page_mock.text)
])
@responses.activate
def test_get_text(client, url, html, expected_text):
    post_text(url)
    quoted_url = quote(url, safe='')
    res = client.get('/text/{}'.format(quoted_url))
    assert res.status_code == 200
    assert res.json['text'] == expected_text
