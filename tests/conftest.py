import pytest
from app import create_app
from crawler.tasks import celery

@pytest.fixture
def client(monkeypatch):
    monkeypatch.setattr('crawler.config.mongo_host', 'mongomock://localhost')
    app = create_app().test_client()
    return app


@pytest.fixture(scope='session')
def celery_config():
    return {
        'broker_url': 'amqp://',
        'result_backend': 'redis://'
    }