import pytest
import json
import responses
from shutil import copy2
import urllib.request
from tests.helpers import page_mock
import tempfile
from urllib.parse import quote
import os
from crawler.services.image import fetch_images_from_page
import crawler.controllers.image
from crawler import config


@pytest.mark.parametrize('url, html', [
    ('https://www.google.com', page_mock.img_html)
])
@responses.activate
def test_fetching_images(monkeypatch, url, html):
    def fake_urlretrive(src, dest):
        copy2('tests/assets/img.png', dest)
    monkeypatch.setattr(urllib.request, 'urlretrieve', fake_urlretrive)
    responses.add(responses.GET, url,
                  body=html, status=200)

    with tempfile.TemporaryDirectory() as tmp:
        monkeypatch.setattr(config, 'images_dir', tmp)
        fetch_images_from_page(url)

        enquoted_url = quote(url, safe='')
        for i in range(0, 2):
            filename = str(i).zfill(5) + '.png'
            filepath = os.path.join(tmp, enquoted_url, filename)
            assert os.path.exists(filepath)


@pytest.mark.parametrize('url, html', [
    ('https://www.google.com', page_mock.img_html)
])
@responses.activate
def test_getting_image(client, monkeypatch, url, html):
    def fake_fetch(url):
        path = os.path.join(config.images_dir, quote(url, safe=''))
        copy2('tests/assets/img.png', dest)
    monkeypatch.setattr(crawler.controllers.image,
                        'fetch_images_from_page', fake_fetch)
    responses.add(responses.GET, url,
                  body=html, status=200)

    with tempfile.TemporaryDirectory() as tmp:
        monkeypatch.setattr(config, 'images_dir', tmp)
        fetch_images_from_page(url)
        res = client.get('/image/{}'.format(url))
        assert res.status_code == 200
