import os
from flask_restplus import abort
import urllib.parse
from crawler.services.image import fetch_images_from_page
from crawler.helpers.image import zip_images_folder
from crawler.helpers.text import normalize_url  


def post_images(url):
        url = normalize_url(url)
        task = fetch_images_from_page.delay(url)
        return {'task_id': task.id}
    

def get_images(url):
    try:
        url = normalize_url(url)
        quoted_url = urllib.parse.quote(url, safe='')
        return zip_images_folder(quoted_url)
    except FileNotFoundError:
        abort(404, 'Requested page was not crawled yet or task failed.')
