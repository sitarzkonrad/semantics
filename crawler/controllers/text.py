from urllib.parse import unquote
from flask_restplus import abort
from celery.utils.log import get_logger
from crawler.models.text import Text
from crawler.helpers.text import normalize_url
from crawler.services.text import download_text
logger = get_logger(__name__)


def post_text(url):
    try:
        return download_text.delay(url)
    except download_text.OperationalError as e:
        logger.exception('Sending to task queue raised {}'.format(str(e)))
        abort(500, 'Server could not process request.')


def get_text(url):
    url = normalize_url(unquote(url))
    obj = Text.objects(url=url).first()
    if obj:
        return obj.text
    else:
        msg = ('Text from this url wasn\'t crawled, '
               'try first to send POST request.')
        abort(404, msg)

