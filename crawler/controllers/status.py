from crawler import tasks
from itertools import chain
from celery import states


def get_status(task_id):
    res = tasks.celery.AsyncResult(task_id)
    status = res.status
    if res.get() == states.FAILURE:
        status = states.FAILURE
    return {'status': status, 'task_id': task_id}
    