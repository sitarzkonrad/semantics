from mongoengine import connect
from crawler import config


def create_connection():
    connect('crawler', host=config.mongo_host, port=config.mongo_port)
