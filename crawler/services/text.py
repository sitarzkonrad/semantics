from bs4 import BeautifulSoup
import requests
import re
import requests
from mongoengine import connect
from urllib.error import HTTPError, URLError

from celery import states
from crawler.tasks import celery
from crawler import config
from crawler.models.text import Text
from crawler.helpers.text import extract_text, isvisible, normalize_url

@celery.task(bind=True)
def download_text(self, url):
    try:    
        con = connect('crawler', host=config.mongo_host, port=config.mongo_port)
        res = requests.get(url,
                           headers={'User-agent': 'Mozilla/5.0'},
                           verify=False)
        text = extract_text(res.text.encode(res.encoding))
        url = normalize_url(url)
        Text(url=url, text=text).save()
        con.close()
        return {'text': text}
    except (requests.exceptions.ConnectionError,
            HTTPError, URLError):
        return states.FAILURE