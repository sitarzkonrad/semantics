import os
from urllib.error import HTTPError, URLError
from celery import states
from bs4 import BeautifulSoup
from urllib.request import urlopen, urlparse, urlretrieve, Request
from urllib.parse import quote
import requests
from celery import states
from crawler.helpers.image import add_extension, get_img_url
from crawler.helpers.text import normalize_url
from crawler.tasks import celery
from crawler import config


@celery.task(bind=True)
def fetch_images_from_page(self, url):
    try:
        url = normalize_url(url)
        html = urlopen(url)
        soup = BeautifulSoup(html, 'html.parser')
        uri = urlparse(url)
        escaped_url = quote(url, safe='')
        dest_folder = os.path.join(config.images_dir, escaped_url)
        os.makedirs(dest_folder, exist_ok=True)
        paths = []
        for index, img in enumerate(soup.findAll("img")):
            filename = str(index).zfill(5)
            outpath = os.path.join(dest_folder, filename)
            img_url = get_img_url(img, uri)
            urlretrieve(img_url, outpath)
            paths.append(add_extension(outpath))
        return paths
    except (HTTPError, URLError):
        return states.FAILURE
