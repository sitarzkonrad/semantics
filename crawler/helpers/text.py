import re
from bs4 import BeautifulSoup


def normalize_url(url):
    if url.endswith('/'):
        url = url[:-1]
    return url


def isvisible(element):
    if element.parent.name in ['style', 'script', '[document]', 'head', 'title']:
        return False
    elif re.match('<!--.*-->', str(element.encode('utf-8'))):
        return False
    return True


def extract_text(html):
    soup = BeautifulSoup(html, 'html.parser')
    for script in soup(["script", "style"]):
        script.decompose()
    text_tags = soup.find_all(text=True)
    text = [x.strip() for x in text_tags if isvisible(x) and x.strip()]
    text = ' '.join(text)
    return text