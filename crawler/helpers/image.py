import io
import os
import zipfile
import magic
from urllib.request import urlopen, urlparse, urlunparse, urlretrieve
from crawler import config
from flask import send_file
from zipfile import ZipFile
from io import BytesIO
import time


def get_img_url(img, uri):
    if img['src'].startswith('//'):
        domain_url = '{scheme}:{src}'.format(
            scheme=uri.scheme, src=img['src'])
        return domain_url
    elif img['src'].startswith('/'):
        domain_url = '{scheme}://{netloc}{src}'.format(
            scheme=uri.scheme, netloc=uri.netloc, src=img['src'])
        return domain_url
    else:
        return img['src']


def add_extension(img_path):
    mime = magic.Magic(mime=True)
    img_type = mime.from_file(img_path).split('/')[1]
    path_with_ext = '{filename}.{type}'.format(
        filename=img_path, type=img_type)
    os.rename(img_path, path_with_ext)
    return path_with_ext


def zip_images_folder(url):
    path = os.path.join(config.images_dir, url)
    memory_file = BytesIO()
    files = get_folder_data(path)
    with zipfile.ZipFile(memory_file, 'w') as zf:
        for img_file in files:
            data = zipfile.ZipInfo(img_file['name'])
            data.date_time = time.localtime(time.time())[:6]
            data.compress_type = zipfile.ZIP_DEFLATED
            zf.writestr(data, img_file['data'])
    memory_file.seek(0)
    return send_file(memory_file,
                     attachment_filename=(url +'.zip'),
                     as_attachment=True)


def get_folder_data(path):
    name_data_list = []
    for file in os.listdir(path):
        with open(os.path.join(path, file), 'rb') as f:
            name_data_list.append({'name': file, 'data': f.read()})
    return name_data_list
