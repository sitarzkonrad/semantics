import os

broker_host = os.environ.get('BROKER_HOST') or 'localhost'
mongo_host = os.environ.get('MONGO_HOST') or 'localhost'
mongo_port = int(os.environ.get('MONGO_PORT')) \
             if os.environ.get('MONGO_PORT') else 27030
images_dir = 'images'
broker_url = 'amqp://user:pass@{}:5672/vhost'.format(broker_host)
backend_url = 'rpc://'
