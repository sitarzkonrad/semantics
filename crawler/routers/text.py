from flask_restplus import Namespace, Resource, inputs
from flask import request, jsonify
from crawler.controllers.text import post_text, get_text


api = Namespace('text',
                description='Download text from specified page with url.')
post_parser = api.parser()
post_parser.add_argument(
    'url',
    type=inputs.URL(schemes=['http', 'https']),
    required=True
)

get_parser = api.parser()
get_parser.add_argument('quoted_url', type=str, required=True, location='args')


@api.route('/')
class Texts(Resource):
    @api.expect(post_parser)
    @api.doc('download text')
    def post(self):
        args = post_parser.parse_args()
        url = args['url']
        task = post_text(url)
        return {'task_id': task.id}


@api.route('/<path:page_url>')
class Text(Resource):
    def get(self, page_url):
        text = get_text(page_url)
        return {'text': text}
