from flask_restplus import Namespace, Resource, inputs
from flask import request
from flask import request, jsonify
from crawler.controllers.image import post_images, get_images


api = Namespace('image', description='Download images from specified page with url.')
post_parser = api.parser()
post_parser.add_argument(
    'url',
    type=inputs.URL(schemes=['http', 'https']),
    required=True
)

get_parser = api.parser()
get_parser.add_argument(
    'img',
    type=str
)

@api.route('')
class Images(Resource):
    @api.expect(post_parser)
    @api.doc('download_images')
    def post(self):
        args = post_parser.parse_args()
        url = args['url']
        status = post_images(url)
        return status


@api.route('/<path:page_url>')
class Image(Resource):
    def get(self, page_url):
        return get_images(page_url)
