from flask_restplus import Namespace, Resource
from flask import request, jsonify
from crawler.controllers.status import get_status


api = Namespace('status', description='Check status of task.')
parser = api.parser()
parser.add_argument(
    'task_id',
    type=str,
    required=True,
    location='args'
)


@api.route('/')
class Status(Resource):
    @api.expect(parser)
    @api.doc('Check status of task')
    def get(self):
        args = parser.parse_args()
        task_id = args['task_id']
        status = get_status(task_id)
        return status
