from celery import Celery
from crawler import config

celery = Celery(__name__, backend=config.backend_url, broker=config.broker_url)
