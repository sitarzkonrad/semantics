from mongoengine import DynamicDocument, StringField


class Text(DynamicDocument):
    url = StringField(primary_key=True)
    text = StringField()
