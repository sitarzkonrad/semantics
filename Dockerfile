FROM python:3.6-alpine
WORKDIR /app
COPY ./requirements.txt .
RUN apk add libmagic
RUN pip install -r requirements.txt --no-cache-dir
COPY . .
ENTRYPOINT [ "python" ]
CMD [ "app.py" ]
